import { combineReducers } from "@reduxjs/toolkit";
import bills from "./bills";

const rootReducer = () =>
  combineReducers({
    bills
  });

export type RootState = ReturnType<ReturnType<typeof rootReducer>>;
export default rootReducer;
