import reducer from "./bills";
import { ADD_BILL, addBill, DELETE_BILL, receiveBills } from "../actions/bills";
import BillsCommunicators from "../communicators/Bills";

const billsCommunicators = new BillsCommunicators();

describe("Bills reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, { type: "" })).toEqual({
      isFetching: false,
      didInvalidate: false,
      lastUpdated: null,
      bills: []
    });
  });

  it("should save Bills", () => {
    expect(
      reducer(undefined, receiveBills(billsCommunicators.getMockBills().bills))
    ).toEqual({
      didInvalidate: false,
      isFetching: false,
      lastUpdated: null,
      bills: billsCommunicators.getMockBills().bills
    });
  });

  it("should add Bills", () => {
    const bills = billsCommunicators.getMockBills().bills;

    const billToAdd = {
      id: 6,
      date: "01-09-2020",
      amount: 199,
      category: "test",
      description: "test"
    };

    bills.push(billToAdd);

    expect(
      reducer(
        {
          didInvalidate: false,
          isFetching: false,
          bills: billsCommunicators.getMockBills().bills
        },
        {
          type: ADD_BILL,
          bills: [billToAdd]
        }
      )
    ).toEqual({
      didInvalidate: false,
      isFetching: false,
      bills
    });
  });

  it("should delete Bills", () => {
    const bills = billsCommunicators.getMockBills().bills;

    bills.splice(0, 1);

    expect(
      reducer(
        {
          didInvalidate: false,
          isFetching: false,
          bills: billsCommunicators.getMockBills().bills
        },
        {
          type: DELETE_BILL,
          mutatedBillIndex: 0
        }
      )
    ).toEqual({
      didInvalidate: false,
      isFetching: false,
      bills
    });
  });
});
