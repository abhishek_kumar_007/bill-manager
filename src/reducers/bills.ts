import {
  ADD_BILL,
  UPDATE_BILL,
  BillsAction,
  DELETE_BILL,
  RECEIVE_BILLS
} from "../actions/bills";
import cloneDeep from "lodash/cloneDeep";
import { Bill } from "../models/Bills";
import { StoreWithFetch } from "../types";

export interface BillsStore extends StoreWithFetch {
  bills: Array<Bill>;
}

export default function wallets(
  state: BillsStore = {
    isFetching: false,
    didInvalidate: false,
    lastUpdated: null,
    bills: []
  },
  action: BillsAction
): BillsStore {
  switch (action.type) {
    case ADD_BILL: {
      let bills = cloneDeep(state.bills);
      if (Array.isArray(action.bills)) {
        bills = bills.concat(action.bills);
      }
      return {
        ...state,
        bills
      };
    }
    case UPDATE_BILL: {
      let bills = cloneDeep(state.bills);
      if (
        action &&
        Array.isArray(action.bills) &&
        typeof action.mutatedBillIndex === "number" &&
        action.mutatedBillIndex < bills.length
      ) {
        bills[action.mutatedBillIndex] = action.bills[0];
      }
      return {
        ...state,
        bills
      };
    }
    case DELETE_BILL: {
      let bills = cloneDeep(state.bills);
      if (
        action &&
        typeof action.mutatedBillIndex === "number" &&
        action.mutatedBillIndex < bills.length
      ) {
        bills.splice(action.mutatedBillIndex, 1);
      }
      return {
        ...state,
        bills
      };
    }
    case RECEIVE_BILLS:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        bills: [...(action.bills || [])]
      };
    default:
      return state;
  }
}
