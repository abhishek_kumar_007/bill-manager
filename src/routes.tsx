import React, { Suspense } from "react";
import { Switch, HashRouter as Router, Route } from "react-router-dom";

const DashBoard = React.lazy(() => import("./pages/Dashboard/Dashboard"));

export default function Routes() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Router>
        <Switch>
          <Route exact path="/">
            <DashBoard />
          </Route>
        </Switch>
      </Router>
    </Suspense>
  );
}
