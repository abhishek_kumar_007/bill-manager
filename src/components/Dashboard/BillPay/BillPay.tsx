import React, { FC } from "react";
import { Bill } from "../../../models/Bills";
import Bills from "../../shared/Bills/Bills";
import NumberField, {
  NumberFieldOnChangeFn
} from "../../shared/NumberField/NumberField";
import styles from "./billPay.module.scss";

export type OnMonthlyBudgetChangeFn = NumberFieldOnChangeFn;

export interface Props {
  bills: Array<Bill>;
  monthlyBudget: number;
  onMonthlyBudgetChange: OnMonthlyBudgetChangeFn;
}

const BillPay: FC<Props> = props => {
  const { bills, monthlyBudget, onMonthlyBudgetChange } = props;

  return (
    <section className={styles.billPaySection}>
      <div className={styles.monthlyBudget}>
        Enter Monthly Budget{" "}
        <NumberField value={monthlyBudget} onChange={onMonthlyBudgetChange} />
      </div>
      <Bills
        bills={bills}
        title="Min bills to pay"
        isEditable={false}
        tableProps={{
          localization: {
            body: {
              emptyDataSourceMessage: "No bill to pay"
            }
          }
        }}
      />
    </section>
  );
};

export default React.memo(BillPay);
