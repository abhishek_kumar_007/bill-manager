import React from "react";
import Enzyme from "enzyme";
import ReactDOM from "react-dom";
import Adapter from "enzyme-adapter-react-16";
import Dashboard from "./Dashboard";
import BillsCommunicator from "../../communicators/Bills";

Enzyme.configure({ adapter: new Adapter() });

const billsCommunicator = new BillsCommunicator();

describe("Dashboard Component", () => {
  it("Render without crash", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <Dashboard
        bills={billsCommunicator.getMockBills().bills}
        billsToPay={[]}
        monthlyBudget={100}
        onBillAdded={() => null}
        onBillUpdated={() => null}
        onBillDeleted={() => null}
        onMonthlyBudgetChange={() => null}
      />,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });
});
