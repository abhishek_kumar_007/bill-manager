import React, { FC } from "react";
import Card from "@material-ui/core/Card";
import BillsModel, { Bill } from "../../../models/Bills";
import LineChart from "../../shared/Highcharts/LineChart/LineChart";

export interface Props {
  bills: Array<Bill>;
}

const MonthlyBillingChart: FC<Props> = (props: Props) => {
  const { bills } = props;

  const billChartData = new BillsModel({
    bills: bills || []
  }).getTimeSeriesData();

  return (
    <Card>
      <LineChart
        data={billChartData}
        config={{
          title: {
            text: "Monthly Bill Spends"
          },
          yAxis: {
            title: {
              text: "Spends"
            }
          },
          time: {
            useUTC: false
          },
          xAxis: {
            type: "datetime"
          }
        }}
      />
    </Card>
  );
};

export default React.memo(MonthlyBillingChart);
