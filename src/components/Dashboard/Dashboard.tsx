import React, { FC } from "react";
import { Bill } from "../../models/Bills";
import Card from "@material-ui/core/Card";
import BillPay, { OnMonthlyBudgetChangeFn } from "./BillPay/BillPay";
import MonthlyBillingChart from "./MonthlyBillingChart/MonthlyBillingChart";
import Bills, {
  ListenerProps as BillsListenerProps
} from "../shared/Bills/Bills";
import styles from "./dashboard.module.scss";

export interface Props extends BillsListenerProps {
  bills: Array<Bill>;
  billsToPay: Array<Bill>;
  showLoader?: boolean;
  monthlyBudget: number;
  onMonthlyBudgetChange: OnMonthlyBudgetChangeFn;
}

const Dashboard: FC<Props> = props => {
  const {
    bills,
    billsToPay,
    showLoader,
    onBillAdded,
    monthlyBudget,
    onBillDeleted,
    onBillUpdated,
    onMonthlyBudgetChange
  } = props;

  return (
    <section className={styles.dashboardSection}>
      <div className={styles.dashboardCard}>
        <MonthlyBillingChart bills={bills} />
      </div>
      <div className={styles.dashboardCard}>
        <Card>
          <Bills
            bills={[...bills]}
            title="User's Bills"
            onBillAdded={onBillAdded}
            onBillDeleted={onBillDeleted}
            onBillUpdated={onBillUpdated}
            isEditable
            tableProps={{
              isLoading: showLoader
            }}
          />
        </Card>
      </div>
      <div className={styles.dashboardCard}>
        <Card>
          <BillPay
            bills={[...billsToPay]}
            monthlyBudget={monthlyBudget}
            onMonthlyBudgetChange={onMonthlyBudgetChange}
          />
        </Card>
      </div>
    </section>
  );
};

Dashboard.defaultProps = {
  showLoader: false
};

export default React.memo(Dashboard);
