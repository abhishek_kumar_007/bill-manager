import cloneDeep from "lodash/cloneDeep";
import React, {FC, useCallback} from "react";
import MaterialTable, {Column, MaterialTableProps} from "material-table";
import {Bill} from "../../../models/Bills";

export type OnBillAddedFn = (bill: Bill) => void;
export type OnBillDeletedFn = (deletedBillIndex: number) => void;
export type OnBillUpdatedFn = (updatedBillIndex: number, bill: Bill) => void;

export interface ListenerProps {
    onBillAdded?: OnBillAddedFn;
    onBillUpdated?: OnBillUpdatedFn;
    onBillDeleted?: OnBillDeletedFn;
}

export interface Props extends ListenerProps {
    bills: Array<Bill>;
    isEditable?: boolean;
    title: string;
    tableProps?: Partial<MaterialTableProps<Array<Bill>>>;
}

const BillTableColumns: Column<Bill>[] = [
    {title: "ID", field: "id", filtering: false},
    {title: "Description", field: "description", filtering: false},
    {title: "Category", field: "category"},
    {title: "Amount", field: "amount", type: "numeric", filtering: false},
    {title: "Date", field: "date", type: "date", filtering: false}
];

const Bills: FC<Props> = (props: Props) => {
    const {
        bills,
        title,
        tableProps,
        onBillAdded,
        onBillDeleted,
        onBillUpdated,
        isEditable = false
    } = props;

    let tableOtherProps = {
        ...tableProps
    };

    const cloneBills = cloneDeep(bills);

    const memoOnBillDeleted = useCallback(
        async (oldData: Bill) => {
            if (typeof onBillDeleted === "function") {
                onBillDeleted(cloneBills.indexOf(oldData));
            }
        },
        [onBillDeleted, cloneBills]
    );

    const memoOnBillAdded = useCallback(
        async (newData: Bill) => {
            if (typeof onBillAdded === "function") {
                onBillAdded(newData);
            }
        },
        [onBillAdded]
    );

    const memoOnBillUpdated = useCallback(
        async (newData: Bill, oldData: Bill) => {
            if (oldData && typeof onBillUpdated === "function") {
                onBillUpdated(cloneBills.indexOf(oldData), newData);
            }
        },
        [onBillUpdated, cloneBills]
    );

    if (isEditable) {
        tableOtherProps = {
            ...tableOtherProps,
            editable: {
                onRowAdd: memoOnBillAdded,
                onRowUpdate: memoOnBillUpdated,
                onRowDelete: memoOnBillDeleted
            }
        };
    }
    return (
        <MaterialTable
            title={title}
            columns={BillTableColumns}
            data={cloneBills}
            options={{
                filtering: true
            }}
            {...tableOtherProps}
        />
    );
};

Bills.defaultProps = {
    isEditable: false
};

export default React.memo(Bills);
