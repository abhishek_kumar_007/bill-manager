import * as React from "react";
import pick from "lodash/pick";
import keys from "lodash/keys";
import isEqual from "lodash/isEqual";
import Highcharts, { Chart } from "highcharts";

export interface Props {
  /**
   * config object can be anything from highcharts
   */
  config?: Object;
  data: Array<any>;
  /**
   * Style attributes
   */
  style?: Object;
  className?: string;
}

class HighchartWrapper extends React.PureComponent<Props> {
  container: React.RefObject<HTMLDivElement>;
  chart: Chart | null;

  static defaultProps = {
    config: {},
    style: null,
    className: null
  };

  constructor(props: Props) {
    super(props);
    this.chart = null;
    this.container = React.createRef<HTMLDivElement>();
  }

  componentDidMount() {
    this.drawChart(this.props);
  }

  componentWillReceiveProps(nextProps: Props) {
    const parsedData = [...this.props.data];
    const dataLength = parsedData.length;

    for (let i = 0; i < dataLength; i++) {
      parsedData[i] = pick(parsedData[i], keys(nextProps.data[i]));
    }

    if (
      !isEqual(nextProps.config, this.props.config) ||
      !isEqual(nextProps.data, parsedData)
    ) {
      this.destroyChart();
      this.drawChart(nextProps);
    }
  }

  destroyChart() {
    if (this.chart) {
      this.chart.destroy();
    }
  }

  componentWillUnmount() {
    this.destroyChart();
  }

  drawChart = (props: Props) => {
    const { config, data } = props;
    if (this.container && this.container.current) {
      this.chart = Highcharts.chart(this.container.current, {
        ...{
          ...config,
          ...{
            series: [...data]
          }
        }
      });
    }
  };

  render() {
    const { style, className } = this.props;

    return <div style={style} className={className} ref={this.container} />;
  }
}

export default HighchartWrapper;
