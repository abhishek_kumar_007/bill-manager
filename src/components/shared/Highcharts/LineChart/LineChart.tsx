import * as React from "react";
import merge from "lodash/merge";
import HighchartWrapper, {
  Props as HighchartWrapperProps
} from "../HighchartWrapper/HighchartWrapper";

const defaultLineConfig = {
  chart: {
    type: "line"
  },
  plotOptions: {
    line: {
      marker: {
        enabled: true,
        symbol: "circle",
        radius: 5,
        lineColor: null
      }
    },
    series: {
      cursor: "pointer"
    }
  },
  legend: {
    enabled: false
  }
};

const LineChart = (props: HighchartWrapperProps) => {
  const { config, data, ...others } = props;

  const combinedConfig = merge(defaultLineConfig, config);

  return <HighchartWrapper config={combinedConfig} data={data} {...others} />;
};

export default LineChart;
