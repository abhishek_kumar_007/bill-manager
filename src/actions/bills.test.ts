import fetchMock from "fetch-mock";
import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";
import * as actions from "./bills";
import BillsCommunicator from "../communicators/Bills";
import { Bill } from "../models/Bills";

const mockStore = configureMockStore([thunk]);

describe("async actions", () => {
  afterEach(() => {
    fetchMock.restore();
  });
  it("Fetch bills", async () => {
    const data = await new BillsCommunicator().getBills();

    const expectedActions = [
      {
        type: actions.RECEIVE_BILLS,
        bills: data.bills
      }
    ];

    const store = mockStore({ bills: [] });

    return store.dispatch<any>(actions.fetchBills()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("Delete a bill", async () => {
    const expectedActions = [
      {
        type: actions.DELETE_BILL,
        mutatedBillIndex: 0
      }
    ];

    const store = mockStore({ bills: [] });

    return store.dispatch<any>(actions.deleteBill(0)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("Add a bill", async () => {
    const bill: Bill = {
      id: 6,
      description: "Test",
      category: "test",
      amount: 100,
      date: "01-02-2020"
    };

    const expectedActions = [
      {
        type: actions.ADD_BILL,
        bills: [bill]
      }
    ];

    const store = mockStore({ bills: [] });

    return store.dispatch<any>(actions.addBill(bill)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
