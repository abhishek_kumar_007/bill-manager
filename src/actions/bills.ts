import {Dispatch, ThunkAction} from "@reduxjs/toolkit";
import {Bill} from "../models/Bills";
import {BaseReduxAction} from "../types";
import {BillsStore} from "../reducers/bills";
import BillsCommunicator from "../communicators/Bills";

export interface BillsAction extends BaseReduxAction {
    bills?: Array<Bill>;
    mutatedBillIndex?: number;
}

export type ThunkResult<R> = ThunkAction<R, BillsStore, null, BillsAction>

export const RECEIVE_BILLS = "RECEIVE_BILLS";
export const UPDATE_BILL = "UPDATE_BILL";
export const DELETE_BILL = "DELETE_BILL";
export const ADD_BILL = "ADD_BILL";

const billsCommunicator = new BillsCommunicator();

export function receiveBills(bills: Array<Bill>): BillsAction {
    return {
        type: RECEIVE_BILLS,
        bills
    };
}

export function addBill(bill: Bill): ThunkResult<Promise<BillsAction>> {
    return async (dispatch: Dispatch<BillsAction>): Promise<BillsAction> => {
        return dispatch({
            type: ADD_BILL,
            bills: [bill]
        });
    };
}

export function deleteBill(billIndex: number): ThunkResult<Promise<BillsAction>> {
    return async (dispatch: Dispatch<BillsAction>): Promise<BillsAction> => {
        return dispatch({
            type: DELETE_BILL,
            mutatedBillIndex: billIndex
        });
    };
}

export function updateBill(billIndex: number, bill: Bill): ThunkResult<Promise<BillsAction>> {
    return async (dispatch: Dispatch<BillsAction>): Promise<BillsAction> => {
        return dispatch({
            type: UPDATE_BILL,
            bills: [bill],
            mutatedBillIndex: billIndex
        });
    };
}

export function fetchBills() {
    return async (dispatch: Dispatch<BillsAction>) => {
        const response = await billsCommunicator.getBills();
        if (response) {
            dispatch(receiveBills(response.bills));
        }
    };
}
