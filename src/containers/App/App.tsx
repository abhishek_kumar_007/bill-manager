import React from "react";
import { Provider } from "react-redux";
import getStore from "../../store/configureStore";
import Header from "../../components/Header/Header";

const App = (props: { children: React.ReactNode }) => {
  const store = getStore();
  return (
    <Provider store={store}>
      <Header />
      <div>{props.children}</div>
    </Provider>
  );
};

export default App;
