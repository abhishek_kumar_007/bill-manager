import React from "react";
import thunk from "redux-thunk";
import fetchMock from "fetch-mock";
import { Provider } from "react-redux";
import Enzyme, { mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import configureMockStore from "redux-mock-store";
import { HashRouter as Router } from "react-router-dom";
import { render, fireEvent } from "@testing-library/react";
import Dashboard from "./Dashboard";
import { RootState } from "../../reducers";
import BillsCommunicator from "../../communicators/Bills";

const billsCommunicator = new BillsCommunicator();

Enzyme.configure({ adapter: new Adapter() });

const mockStore = configureMockStore<RootState>([thunk]);

describe("Dashboard App", () => {
  afterEach(() => {
    fetchMock.restore();
  });

  it("Should render Dashboard component", () => {
    const mockBills = billsCommunicator.getMockBills();

    const store = mockStore({
      bills: { bills: mockBills.bills, isFetching: false, didInvalidate: false }
    });

    const wrapper = mount(
      <Provider store={store}>
        <Router>
          <Dashboard />
        </Router>
      </Provider>
    );

    const bills = wrapper
      .childAt(0)
      .childAt(0)
      .childAt(0)
      .childAt(0)
      .prop("bills");
    expect(bills).toEqual(mockBills.bills);
  });

  it("Check for Monthly bill to pay", () => {
    const mockBills = billsCommunicator.getMockBills();

    const store = mockStore({
      bills: { bills: mockBills.bills, isFetching: false, didInvalidate: false }
    });

    const { getAllByTestId } = render(
      <Provider store={store}>
        <Router>
          <Dashboard />
        </Router>
      </Provider>
    );

    const node = getAllByTestId("input");
    // setting monthly budget to 50000
    fireEvent.change(node[0], { target: { value: "50000" } });

    // @ts-ignore
    // value should be 50000
    expect(node[0].value).toEqual("50000");
  });
});
