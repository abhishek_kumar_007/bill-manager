import React, {FC, useCallback, useEffect, useState} from "react";
import {createSelector} from "reselect";
import {RootState} from "../../reducers";
import {BillsStore} from "../../reducers/bills";
import {useDispatch, useSelector} from "react-redux";
import BillsModel, {Bill} from "../../models/Bills";
import DashboardView from "../../components/Dashboard/Dashboard";
import {
    addBill,
    deleteBill,
    fetchBills,
    updateBill
} from "../../actions/bills";

const selectBills = createSelector<RootState, BillsStore, BillsStore>(
    state => state.bills,
    billStore => billStore
);

const Dashboard: FC = () => {
    const dispatch = useDispatch();
    const [monthlyBudget, setMonthlyBudget] = useState<number>(0);
    const [billsToPay, setBillsToPay] = useState<Bill[]>([]);

    const billStore = useSelector(selectBills);

    // effect to fetch bills
    useEffect(() => {
        dispatch(fetchBills());
    }, [dispatch]);

    useEffect(() => {
        const billsModel = new BillsModel({bills: [...billStore.bills]});
        const billToPay = billsModel.getMinBillToPay(monthlyBudget);
        setBillsToPay(billToPay);
    }, [monthlyBudget, billStore]);

    const memoOnMonthlyBudgetChange = useCallback((event, value) => {
        setMonthlyBudget(value);
    }, []);

    const memoOnBillAdded = useCallback(
        (bill: Bill) => {
            setMonthlyBudget(0);
            bill.amount = Number(bill.amount);
            dispatch(addBill({...bill}));
        },
        [dispatch]
    );

    const memoOnBillUpdated = useCallback(
        (updatedBillIndex: number, bill: Bill) => {
            setMonthlyBudget(0);
            bill.amount = Number(bill.amount);
            dispatch(updateBill(updatedBillIndex, {...bill}));
        },
        [dispatch]
    );

    const memoOnBillDeleted = useCallback(
        (updatedBillIndex: number) => {
            setMonthlyBudget(0);
            dispatch(deleteBill(updatedBillIndex));
        },
        [dispatch]
    );

    return (
        <DashboardView
            bills={[...billStore.bills]}
            billsToPay={billsToPay}
            onBillAdded={memoOnBillAdded}
            monthlyBudget={monthlyBudget}
            onBillUpdated={memoOnBillUpdated}
            onBillDeleted={memoOnBillDeleted}
            onMonthlyBudgetChange={memoOnMonthlyBudgetChange}
        />
    );
};

export default Dashboard;
