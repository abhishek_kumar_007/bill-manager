export interface Bill {
  id: number;
  description: string;
  category: string;
  amount: number;
  date: string;
}

export interface BillsOptions {
  bills: Array<Bill>;
}

class Bills {
  _bills: Array<Bill>;

  constructor({ bills }: BillsOptions) {
    this._bills = bills || [];
  }

  getTimeSeriesData = () => {
    let highChartData = [];

    if (this._bills && this._bills.length) {
      const hashMap: { [key: number]: number } = {};
      let data = [];
      for (let i = 0; i < this._bills.length; i++) {
        const epochTime = Date.parse(this._bills[i].date);
        hashMap[epochTime] = (hashMap[epochTime] || 0) + this._bills[i].amount;
      }

      for (const dataKey in hashMap) {
        if (Object.prototype.hasOwnProperty.call(hashMap, dataKey)) {
          data.push([Number(dataKey), hashMap[dataKey]]);
        }
      }

      data = data.sort((t1, t2) => {
        return t1[0] - t2[0];
      });

      highChartData.push({
        name: "Spends",
        data
      });
    }

    return highChartData;
  };

  getMinBillToPay = (monthlyBudget: number): Array<Bill> => {
    let remainingAmount = monthlyBudget;
    const bills = this._bills.sort((b1, b2) => {
      return b2.amount - b1.amount;
    });

    const billsToPay = [];

    for (let i = 0; i < bills.length; i++) {
      const bill = bills[i];
      if (bill.amount <= remainingAmount) {
        billsToPay.push(bill);
        remainingAmount -= bill.amount;
      }
    }

    return billsToPay;
  };
}

export default Bills;
