import React from "react";
import Dashboard from "../../containers/Dashboard/Dashboard";

function DashboardPage() {
  return <Dashboard />;
}

export default DashboardPage;
