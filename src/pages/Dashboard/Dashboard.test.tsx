import React from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router } from "react-router-dom";
import Dashboard from "./Dashboard";
import App from "../../containers/App/App";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <App>
      <Router>
        <Dashboard />
      </Router>
    </App>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
